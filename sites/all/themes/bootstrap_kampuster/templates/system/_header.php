<script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/fee142e67c33423bab72694eb/abc5558e77e5444193788b2dd.js");</script>
<header id="navbar" role="banner" class="<?php print $navbar_classes; ?>">
	<div class="<?php print $container_class; ?>" id="menu-container">
		<div class="navbar-header">
			<?php if (!empty($logo)): ?>
				<a class="logo" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
					<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
				</a>
			<?php else: ?>
				<?php if (!empty($site_name)): ?>
					<a class="name navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
				<?php endif; ?>
			<?php endif; ?>

			<?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
				<a class="navbar-toggle" data-toggle="collapse" href="#navbar-collapse">
					<span class="sr-only"><?php print t('Toggle navigation'); ?></span>
					<span class="glyphicon glyphicon-menu-hamburger" style="color:#eee;"></span>
				</a>
				<a class="navbar-toggle" data-toggle="collapse" href="#userbar-collapse">
					<span class="glyphicon glyphicon-user" style="color:#eee;"></span>
				</a>
				<a class="navbar-toggle" data-toggle="collapse" href="#langbar-collapse">
					<span class="glyphicon glyphicon-globe" style="color:#eee;"></span>
				</a>
			<?php endif; ?>
		</div>
		<div class="navbar-collapse collapse" id="navbar-collapse">
			<?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
				<nav role="navigation">
					<?php if (!empty($primary_nav)): ?>
						<?php print render($primary_nav); ?>
					<?php endif; ?>
					<?php if (!empty($page['navigation'])): ?>
						<?php print render($page['navigation']); ?>
					<?php endif; ?>
					<ul class="nav navbar-nav navbar-right hidden-xs">
						<?php if (user_is_logged_in()): ?>
							<li class="dropdown-user-menu">
								<a href="" class="dropdown-toggle" data-toggle="dropdown">
									<?php print str_replace('class=', 'class="img-responsive img-circle sm-img" href=', $user_image); ?>
									<?php print $user_first; ?> <?php print $user_last; ?>
									<span class="glyphicon glyphicon-chevron-down"></span>
								</a>
								<div class='dropdown-menu navbar-login'>
									<div class='row'>
										<div class='col-lg-4'>
											<?php print str_replace('class=', 'class="img-responsive img-circle lg-img" href=', $user_image); ?>
										</div>
										<div class='col-lg-8'>
											<div class="text-left"><strong><?php print $user_first . ' ' . $user_last; ?></strong></div>
											<div class="text-left small"><?php print $user_mail; ?></div>
											<?php print theme('links__system_secondary_menu', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary-menu', 'class' => 'navbar-nav-user'))); ?>
										</div>
									</div>
								</div>
							</li>
						 <?php else : ?> 
							<li>
								<?php
									$menu = theme('links__system_secondary_menu', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary-menu')));
									print str_replace('a href=', 'a class="btn btn-border-w" href=', $menu);
								?>
							</li>  
						<?php endif; ?>
					</ul>
				</nav>
			<?php endif; ?>
		</div>

		<!-- USER MENU FOR MOBILE -->
				<div class="visible-xs-block">
					<div class="collapse" id="userbar-collapse">
						<?php if (user_is_logged_in()): ?>
						<div class='navbar-login'>
							<div class='row'>
								<div class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>
									<?php print str_replace('class=', 'class="img-responsive img-circle sm-img" href=', $user_profile); ?>
								</div>
								<div class='col-xs-8 col-sm-8 col-md-8 col-lg-8'>
									<div class="text-left"><strong><?php print $user_first . ' ' . $user_last; ?></strong></div>
									<div class="text-left small"><?php print $user_mail; ?></div>
									<?php print theme('links__system_secondary_menu', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary-menu', 'class' => 'navbar-nav-user'))); ?>
								</div>
							</div>
						</div>
						 <?php else : ?> 
							<?php
								$menu = theme('links__system_secondary_menu', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary-menu', 'class' => 'navbar-nav-user')));
								print str_replace('a href=', 'a class="btn btn-border-w" href=', $menu);
							?>
						<?php endif; ?>
					</div>
				</div>
			<!-- end -->

	<!-- end -->
	</div>
</header>