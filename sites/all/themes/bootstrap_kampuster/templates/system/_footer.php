<?php if (!empty($page['footer'])): ?>
<footer class="footer">
    <div class="<?php print $container_class; ?>">
        <div class="row">
            <?php print render($page['footer']); ?>
        </div>
    </div>
</footer>
<?php endif; ?>