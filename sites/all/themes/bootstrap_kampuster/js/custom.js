(function($){
 $(document).ready(function(){
  'use strict';
  //============================== COUNTER-UP =========================
  $('.counter').counterUp({
    delay: 10,
    time: 2000
  });

    $('.banerInfo h1.firstDesc')
      .textillate({ initialDelay: 1000, in: { effect: 'fadeIn' }});
    $('.banerInfo h1.secondDesc')
      .textillate({ initialDelay: 3000, in: { effect: 'fadeIn' }});
    $('.banerInfo h1.thirdDesc')
      .textillate({ initialDelay: 5000, in: { effect: 'fadeIn' }});
    $('.banerInfo h1.fourthDesc')
      .textillate({ initialDelay: 7000, in: { effect: 'fadeIn' }});
    $('.banerInfo p.firstDesc')
      .textillate({ initialDelay: 2000, in: { delay: 3, shuffle: 'fadeIn' } });
    $('.banerInfo p.secondDesc')
      .textillate({ initialDelay: 4000, in: { delay: 3, shuffle: 'fadeIn' } });
    $('.banerInfo p.thirdDesc')
      .textillate({ initialDelay: 6000, in: { delay: 3, shuffle: 'fadeIn' } });
    $('.banerInfo p.fourthDesc')
      .textillate({ initialDelay: 8000, in: { delay: 3, shuffle: 'fadeIn' } });

 });



})(jQuery);

/**jQuery(document).ready(function(){
  'use strict';
  //============================== COUNTER-UP =========================
  jQuery('.counter').counterUp({
    delay: 10,
    time: 2000
  });
});**/
