<?php

/**
 * @file
 * The primary PHP file for this theme.
 */


/**
 * Get language-switcher-locale content.
 */
function bootstrap_kampuster_locale() {
  $block_language = module_invoke('locale', 'block_view', 'language');
  $block_language = str_replace('language-link', 'language-link btn btn-default', $block_language['content']);
  $block_language = str_replace('<li class="', '<li class="btn ', $block_language);
  return $block_language;
}

/**
 * Implements template_preprocess_page().
 */
function bootstrap_kampuster_preprocess_page(&$vars) {
  global $user;
  if (user_is_logged_in()) {
    $user_profile = entity_metadata_wrapper('user', $user->uid);
    $first = isset($user_profile->profile_main) ? $user_profile->profile_main->field_first_name->value() : $user->name;

    // If empty profile name to set user account name.
    $first = empty($first) ? $user->name : $first;
    $last = isset($user_profile->profile_main) ? $user_profile->profile_main->field_last_name->value() : '';
    $uri = isset($user->picture) & $user->picture > 0 ? file_load($user->picture)->uri : 'public://default_images/user_placeholder.png';

    $vars['user_first'] = $first;
    $vars['user_last'] = $last;
    $vars['user_image'] = '';
    $vars['user_mail'] = $user->mail;
    if ($uri) {
        $image = array(
        'style_name' => 'medium',
        'path' => $uri,
      );
      $vars['user_image'] = theme('image_style', $image);
    }
  }
  else {
    $vars['user_first'] = '';
    $vars['user_last'] = '';
    $vars['user_image'] = '';
  }
}

/**
 * Debug entity.
 */
function _wrapper_debug($w) {
  $values = array();
  foreach ($w->getPropertyInfo() as $key => $val) {
    $values[$key] = $w->$key->value();
  }
  return kpr($values);
}
